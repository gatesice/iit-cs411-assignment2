////////////////////////////////////////////////////////////////////////////////
//
// CS411 - f13
//
// Assignment 2 support: scanfill raster graphics program skeleton (snake game)
//
////////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "GL/glut.h"

// Data structures are moved to mycode.h
#include "mycode.h"

////////////////////////////////////////////////////////////////////////////////
// global variables
////////////////////////////////////////////////////////////////////////////////

Snake   S={1,           // snake length
           5,           // snake width
           3,           // snake speed
           1,           // speed increment
           1,           // width increment
           0,0,255};    // snake color

Context C={0,0,0,       // fbuff
           0,0,0,       // texture image
           0,0,0,       // fillColor
           255,255,255, // clearColor
           30,          // frames per second
           0,           // frame number
           0.25,        // number of seconds for length increase
           10,          // number of seconds for speed increase
           20,          // number of seconds for width increase
           0,           // display mode
           0,           // game over flag
           0};          // board directions array



////////////////////////////////////////////////////////////////////////////////
// Object management functions
////////////////////////////////////////////////////////////////////////////////

int readObjLst(char *fn, Obj *objLst) // same as in assignment 1
{
  FILE* fin;
  char c,c1,c2;
  int i;
  Obj *o;

  int objNum=0;

  // set default values
  Obj curDefault= {NOOBJ, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0, 0.0};

  // open input file
  if(! (fin=fopen(fn,"r")) ){
    printf("\nCan't open file %s\n\7",fn);
    return 0;
  }

  // read objects
  while(fscanf(fin,"%c%c",&c1,&c2)!=EOF) {

    // remove white space
    while(c1== ' ' || c1 =='\n'){
      c1=c2;
      if(fscanf(fin,"%c",&c2)==EOF) return(objNum);
    }
    switch(c1){

    // add object
    case 'a':
      o = &objLst[objNum];
      *o = curDefault;

      switch(c2){
      case 'l': // polyline
      case 'p': // polygon
	o->type=PLINE;
	if(c2=='p') o->type=PGON;
	fscanf(fin,"%d",&o->ptsNum);
	for(i=0;i<o->ptsNum;i++)fscanf(fin,"%f%f",&o->pts[i].x,&o->pts[i].y);
	objNum++; 
	break;
      case 'r': // Rectangle
	o->type=RECTOBJ;
	fscanf(fin,"%f%f%f%f",&o->pts[0].x,&o->pts[0].y,&o->pts[1].x,&o->pts[1].y);
	o->ptsNum=2;
	objNum++; 
	break;
      case 'c': // circle
	o->type=CIRC;
	fscanf(fin,"%f%f%f",&o->pts[0].x,&o->pts[0].y,&o->param); 
	o->ptsNum=1;
	objNum++; 
	break;
      default:
	printf("Warning: unknown object type (%c)\n\7",c2);
      }
      break;

    // set property
    case 's':
      o = &curDefault;

      switch(c2){
      case 't': fscanf(fin,"%f%f",&o->tx,&o->ty);       break; // translation
      case 'r': fscanf(fin,"%f",&o->rotAng);            break; // rotation
      case 's': fscanf(fin,"%f%f",&o->sx,&o->sy);       break; // scale
      case 'c': fscanf(fin,"%f%f%f",&o->r,&o->g,&o->b); break; // color
      case 'f': fscanf(fin,"%f",&o->fillFlag);          break; // fill
      default:
	printf("Warning: unknown object property (%c)\n\7",c2);
      }
      break;

    }

    // remove trailing characters to end of line
    while(fread(&c,1,1,fin)) if (c== '\n') break;
  }

  // return number of objects read
  fclose(fin);
  return(objNum);
}


void dumpObjLst(int objNum, Obj *objLst)
{
  int i,j;
  Obj *o;

  // print the opbect type and its points
  for(i=0;i<objNum;i++){
    o = &objLst[i];

    printf("[OBJECT] *** %d:", o->type);
    for(j=0;j<o->ptsNum;j++) printf(" (%3.1f,%3.1f)",o->pts[j].x, o->pts[j].y);
    printf("\n");
  }
}


////////////////////////////////////////////////////////////////////////////////
// Image/pixel handling functions
////////////////////////////////////////////////////////////////////////////////

void setFillColor(int r, int g, int b) {
	RGBPixel pix;
	pix.r = r;
	pix.g = g;
	pix.b = b;
	C.fillColor = pix;
}

//
// Set pixel (x,y) in the frame buffer to the current fill color. Assume
// a coordinate system with (0,0) at the lower left corner that extends
// to (FBUFF_WIDTH-1) in x and to (FBUFF_HEIGHT-1) in y. Perform range
// check to avoid illegal memory access.
//
inline void setPixel(int x, int y)
{
  // check if the frame buffer was allocated
  if(!C.fbuff.data) {
    fprintf(stderr,"Error: access to non-allocated buffer\n\7");
    return;
  }

  // check range
  if(x<0 || x>= C.fbuff.cols || y<0 || y>=C.fbuff.rows) return;

  // fill pixel. Since the image is inverted by glDrawPixels y-inversion should be avoided.
  // C.fbuff.data[FBUFF_HEIGHT-y][x]=fillColor;
  C.fbuff.data[y][x]=C.fillColor;
}


inline RGBPixel getPixel(int x, int y)
{
  // check if the frame buffer was allocated
  if(!C.fbuff.data) {
    fprintf(stderr,"Error: access to non-allocated buffer\n\7");
    return(C.clearColor);
  }

  // check range
  if(x<0 || x>= C.fbuff.cols || y<0 || y>=C.fbuff.rows) return(C.clearColor);

  // get pixel
  ///return(C.fbuff.data[C.fbuff.rows-y][x]);
  return(C.fbuff.data[y][x]);
}


void renderStrokeString(char *string)
{
  glPushMatrix();
  glScalef(0.25,0.25,1.0);
  char *c;
  // loop over all the characters in the string
  for (c=string; *c != '\0'; c++) {
    glutStrokeCharacter(GLUT_STROKE_ROMAN, *c);
  }
  glPopMatrix();
}


void deallocRGBImg(RGBImg arr, int rows)
{
  // deallocate rows
  delete[] arr[0];

  // deallocate row pointers
  delete[] arr;
}


RGBImg allocRGBImg(int rows, int cols)
{
  RGBPixel **arr;
  RGBPixel *data;
  int i;

  // allocate row pointers
  if( !(arr = new RGBPixel*[rows])) return 0;


  // allocate rows 
  if( !(data = new RGBPixel[rows*cols])){
    deallocRGBImg(arr, rows);
    return 0;
  }

  // assign row pointers
  for(i=0;i<rows;i++) arr[i] = data + i*cols;

  return(arr);
}


void bgr2rgb(int rows, int cols, RGBImg img)
{
  int i,j;
  unsigned char tmp;
 
  for(i=0;i<rows;i++) for(j=0;j<cols;j++){
    tmp = img[i][j].r;
    img[i][j].r = img[i][j].b;
    img[i][j].b = tmp;
  }
}


RGBImg readTGAImg(char *filename, int *rows, int *cols)
{
  int size, i,j;
  char	head[18] ;
  unsigned char *ptr;
  FILE	*tga_in;
  RGBImg data;
  
  // open input file
  if(!(tga_in = fopen(filename, "rb" ))) return 0;
    
  // read TARGA file header
  fread( head, sizeof(char), 18, tga_in ) ;
  *cols = (int)((unsigned int)head[12]&0xFF | (unsigned int)head[13]*256);
  *rows = (int)((unsigned int)head[14]&0xFF | (unsigned int)head[15]*256);

  // check the header content
  printf("TGA Image size %d x %d\n", *cols, *rows);
  if(head[2] != 2)
    {
      printf("Image type is %d, it is not true color, uncompressed\n",
	     head[2]);
      fclose(tga_in);
      return(NULL);
    }
  
  if(head[16] != 24)
    {
      printf("Number of bits per pixel is %d != 24 \n", head[16]);
      fclose(tga_in);
      return(NULL);
    }
  
  // allocate image buffer
  size = (*cols) * (*rows);
  if( !(data = allocRGBImg(*rows, *cols)) )
    {
      fprintf(stderr, "Unable to allocate (%d x %d) array\n", *cols, *rows);
      fclose(tga_in);
      return(NULL);
    }

  // check if the image is mirrored (about a horizontal line)
  if(head[17]&0x20)
    {
      // read image data
      for(i = 0; i < *rows; i++)
	fread(data[*rows-1-i], 3*sizeof(char), *cols, tga_in);
    }
  else 
    {
      // read image data
      for(i = 0; i < *rows; i++)
	fread(data[i], 3*sizeof(char), *cols, tga_in);
    }

  fclose(tga_in) ;
  bgr2rgb(*rows, *cols, data);
  return(data);
}	



///////////////////////////////////////////////////////////////////////////////
// raster functions
///////////////////////////////////////////////////////////////////////////////

#define ROUND(a) ((int)(a+0.5))

void drawLineDDA(Point &s, Point &e) // from Hearn and Baker
{
  int xa=int(s.x), ya=int(s.y);
  int xb=int(e.x), yb=int(e.y);
  //  printf("drawing line (%d,%d) -> (%d,%d)\n",xa,ya,xb,yb);

  int dx = xb - xa, dy = yb - ya, steps, k;
  float xIncrement, yIncrement, x = xa, y = ya;

  if (abs (dx) > abs (dy)) steps = abs (dx);
  else steps = abs (dy);
  xIncrement = dx / (float) steps;
  yIncrement = dy / (float) steps;
    
  setPixel (ROUND(x), ROUND(y));
  for (k=0; k<steps; k++) {
    x += xIncrement;
    y += yIncrement;
    setPixel (ROUND(x), ROUND(y));
  }
}


void drawTriangle(Obj *o)
{
  int i;

  RGBPixel colorTab[6]={
                     255,   0,   0,
                     0,   255,   0,
                     0,     0, 255,
                     100, 100,   0,
                       0, 100, 100,
                     100,   0, 100};

  // draw the triangle with alternating edge colors
  for(i=0; i < o->ptsNum -1 ;i++){
    C.fillColor=colorTab[i%6];          // change the fill color
    drawLineDDA(o->pts[i],o->pts[i+1]); // draw an edge
  }
  C.fillColor=colorTab[i%6];            // change the fill color
  drawLineDDA(o->pts[i],o->pts[0]);
}




////////////////////////////////////////////////////////////////////////
// main program
////////////////////////////////////////////////////////////////////////

inline void clearBoard()
{
  int i,j;

  // clear the frame buffer
  for(i=0;i<C.fbuff.rows;i++) for(j=0;j<C.fbuff.cols;j++) 
    C.fbuff.data[i][j]=C.clearColor; 
}


inline void initBoard(int h, int w)
{
  int i,j;
  RGBPixel *data;
  int *idata;

  // allocate the frame buffer
  C.fbuff.rows=h;
  C.fbuff.cols=w;
  C.fbuff.data=new RGBPixel*[h];
  data=new RGBPixel[w*h]; // MODIFIED!
  for(i=0;i<h;i++) C.fbuff.data[i]=data + i*w; // MODIFIED!
  clearBoard();

  // allocate board directions array
  C.dirs=new int*[h];
  idata=new int[w*h];
  for(i=0;i<h;i++) C.dirs[i]=idata + i*w;
  for(i=0;i<h;i++) for(j=0;j<w;j++) C.dirs[i][j]=NIL;

  // initialize the snake
  S.pts[0].x=int(w/2);
  S.pts[0].y=0;
  C.dirs[int(S.pts[0].y)][int(S.pts[0].x)]=DOWN;
}


void testCollision()
{
  int i,j;
  RGBPixel pix;
  int x,y;
  int k;
  int collisionDetected=0;

  // test for collision with triangles
  setFillColor(S.color.r,S.color.g,S.color.b);
  for(k=0; k< S.len; k++)
  {
    for(i=-S.width;i<S.width;i++) for(j=-S.width;j<S.width;j++){
      x=S.pts[k].x+i;
      y=S.pts[k].y+j;
      if(x>=C.fbuff.cols) x-=C.fbuff.cols;
      if(x<0) x+=C.fbuff.cols;
      if(y>=C.fbuff.rows) y-=C.fbuff.rows;
      if(y<0) y+=C.fbuff.rows;
      pix=getPixel(x,y);
      if (pix.r != C.clearColor.r || pix.g != C.clearColor.g || pix.b != C.clearColor.b)
        collisionDetected=1;
    }
  }

  // test for colision with other snake parts
  for(k=1; k< S.len; k++)
  {
    if(S.len>2 && S.pts[0].x==S.pts[k].x && S.pts[0].y==S.pts[k].y)
      collisionDetected=1;
  }

  // handle collision
  if(collisionDetected){
    setFillColor(255, 0, 0);
    glutIdleFunc(0);  
    C.gameOver=1;
  }
}


void drawSnake()
{
  int i,j,x,y, k;

  // draw the snake
  for(k=0; k< S.len; k++)
  {
    for(i=-S.width;i<S.width;i++) for(j=-S.width;j<S.width;j++){
      x=S.pts[k].x+i;
      y=S.pts[k].y+j;
      if(x>=C.fbuff.cols) x-=C.fbuff.cols;
      if(x<0) x+=C.fbuff.cols;
      if(y>=C.fbuff.rows) y-=C.fbuff.rows;
      if(y<0) y+=C.fbuff.rows;
      setPixel(x,y);
    }
  }


}


void enlargeSnake()
{
  if(S.len==10000) return;

  // replicate the snake tail
  S.pts[S.len].x=S.pts[S.len-1].x;
  S.pts[S.len].y=S.pts[S.len-1].y;
  S.len++;  
}


void moveSnake()
{
  int k;
  float prevx,prevy;
  int prevdir;

  prevdir=C.dirs[int(S.pts[0].y)][int(S.pts[0].x)];
  for(k=0; k< S.len; k++)
  {
    // do not move a newly added link
    if(k!=0 && S.pts[k].x==prevx && S.pts[k].y==prevy) continue;
    prevx=S.pts[k].x;
    prevy=S.pts[k].y;

    // move the snake links
    switch (C.dirs[int(S.pts[k].y)][int(S.pts[k].x)]) {
      case LEFT:
        S.pts[k].x-=S.speed;
        if (S.pts[k].x <0) S.pts[k].x=C.fbuff.cols-1; 
        break;
      case RIGHT:
        S.pts[k].x+=S.speed;
        if (S.pts[k].x >C.fbuff.cols-1) S.pts[k].x=0; 
        break;
      case UP:
        S.pts[k].y+=S.speed;
        if (S.pts[k].y >C.fbuff.rows-1) S.pts[k].y=0; 
        break;
      case DOWN:
        S.pts[k].y-=S.speed;
        if (S.pts[k].y <0) S.pts[k].y=C.fbuff.rows-1; 
        break;
    }
    // update the direction of the new snake head
    if(k==0) C.dirs[int(S.pts[0].y)][int(S.pts[0].x)]=prevdir;
  }
}



void reshape(int w, int h)
{
  glViewport (0, 0, w, h);
}


void display(void)
{
  int i;
  Obj *o;
  char str[512];

  // initialize modelview and projection transformations
  glClearColor (0.0, 0.0, 0.0, 0.0);
  glClear (GL_COLOR_BUFFER_BIT);
  glMatrixMode (GL_PROJECTION);
  glLoadIdentity ();
  glOrtho(0, C.fbuff.cols, 0.0, C.fbuff.rows, 0, 10.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity ();

  // star with a clear board
  clearBoard();

  // scan the list of objects and draw only filled/unfilled polygons
  // ignore all object attributes
  for (i=0;i<C.objNum;i++) 
  {
    o = &(C.objLst[i]);
    if(o->type != PGON) continue;

	if(C.displayMode == displayMode::TEXTURE) drawTriangleImage(o);
    else drawFilledTriangle(o);
  }

  // test for collision and draw the snake
  testCollision();
  drawSnake();
  
  // display the frame buffer
  glRasterPos2f(0, 0);
  glDrawPixels(C.fbuff.cols, C.fbuff.rows, GL_RGB, GL_UNSIGNED_BYTE, C.fbuff.data[0]);

  // display the score
  glPushMatrix();
  glColor3f(0.0,0.0,0.0);
  glTranslatef(10.0,10.0,0.0);
  sprintf(str,"score: %d",C.frameNum);
  renderStrokeString(str);
  glPopMatrix();

  // swap buffers
  glFlush();
  glutSwapBuffers();

  title();
}


void idle(void)
{
  static int prevTime=glutGet(GLUT_ELAPSED_TIME);
  int currentTime = glutGet(GLUT_ELAPSED_TIME);
  int elapsed = currentTime-prevTime; // time difference [ms]

  // check if a new frame is required
  if (elapsed < (1000.0/C.fps)) return; // FPS is the desired frame rate

  // update the previous rendering time
  prevTime=currentTime;

  // move the snake
  moveSnake();


  // update the snake width and length if ncessary
  C.frameNum++;
  if (!(C.frameNum % int(C.event1rate*C.fps))) enlargeSnake();
  //if (!(C.frameNum % int(C.event2rate*C.fps))) S.speed+=S.speedIncr;
  //if (!(C.frameNum % int(C.event3rate*C.fps))) S.width+=S.widthIncr;

  // update the display
  glutPostRedisplay();

	title();
}


void keyboard(unsigned char key, int x, int y)
{
  switch(key){
  case 27: exit(0); // ESC
  case 'h': case 'H': printf("\nPress t to toggle display mode\nPress f to toggle fill mode\n"); break;
  case 't': case 'T': changeDisplayMode(); break;
  case 'f': case 'F': changeFillMode(); break;
  }

  glutPostRedisplay();
}


void special(int key, int x, int y)
{
  int xx,yy,curdir;

  xx=S.pts[0].x;
  yy=S.pts[0].y;
  curdir=C.dirs[yy][xx];
  switch (key) {
    case GLUT_KEY_LEFT:  if(curdir!=RIGHT) C.dirs[yy][xx]=LEFT; break;
    case GLUT_KEY_RIGHT: if(curdir!=LEFT)  C.dirs[yy][xx]=RIGHT; break;
    case GLUT_KEY_UP:    if(curdir!=DOWN)  C.dirs[yy][xx]=UP; break;
    case GLUT_KEY_DOWN:  if(curdir!=UP)    C.dirs[yy][xx]=DOWN; break;
  }
  glutPostRedisplay();
}


int main(int argc, char *argv[])
{
  // check transferred arguments
  if(argc<3){
    printf("\nUsage snake <polygonFile> <imageFile>\n\n");
    exit(0);
  }

  // load, dump, and process the object file
  C.objNum=readObjLst(argv[1],C.objLst);
  dumpObjLst(C.objNum, C.objLst);

  // load the TGA image
  if(!(C.texture.data=readTGAImg(argv[2], &C.texture.rows, &C.texture.cols)))
  {
    printf("Could not open %s\n\7",argv[2]);
    exit(0);
  }

  // create a raster display that is handled by myDisplay and myKeyboard
  initBoard(C.texture.rows, C.texture.cols);

  // create the openGL window
  glutInit(&argc, argv);
  glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
  glutInitWindowSize (C.fbuff.cols, C.fbuff.rows); 
  glutInitWindowPosition (100, 100);
  glutCreateWindow("snake");
  glutDisplayFunc(display); 
  glutReshapeFunc(reshape);
  glutKeyboardFunc(keyboard);
  glutSpecialFunc(special);
  glutIdleFunc(idle);
  glutMainLoop();

  return 0;
}

////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////
