#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "GL/glut.h"
#include "mycode.h"

// extern functions in main.cpp
extern void drawTriangle(Obj *);
extern void solidFill(int, int);
extern void imageFill(int, int);
extern void stripeFill(int, int);
extern Context C;
extern Snake S;
extern void setFillColor(int, int, int);
//////////////////////////////////////////////////////////////////////
// Code implementation
//////////////////////////////////////////////////////////////////////

bool boundCheck(int x, int y) {
	if (x < 0 || y < 0) { return false; }
	if (x >= C.fbuff.cols || y >= C.fbuff.rows) { return false;	}
	if (x >= C.texture.cols || y >= C.texture.rows) { return false; }
	return true;
}

void solidFill(int x, int y) {
	if (boundCheck(x, y)) {
		setFillColor(0,0,0);
		C.fbuff.data[y][x] = C.fillColor;
	}
}

void imageFill(int x, int y) {
	if (boundCheck(x, y)) {
		C.fillColor=C.texture.data[y][x];
		C.fbuff.data[y][x] = C.fillColor;
	}
}

void stripeFill(int x, int y) {
	if (boundCheck(x, y)) {
		RGBPixel colorTab[6]={
			255,   0,   0,
			0,   255,   0,
			0,     0, 255,
			100, 100,   0,
			0,   100, 100,
			100,   0, 100};
		int c = y % 6;
		setFillColor(colorTab[c].r,colorTab[c].g,colorTab[c].b);
		C.fbuff.data[y][x] = C.fillColor;
	}
}

// Calculate the intersects of scanline and the triangle
// 
void calcX(const Point &min, const Point &max, const Point &mid, const float &scanline_y, float &xmin, float &xmax) {
	float u1, u2, x1_e, x2_e;

	if (scanline_y > mid.y) {
		u1 = (scanline_y - mid.y) / (max.y - mid.y);
		x1_e = mid.x + u1 * (max.x - mid.x);
	} else if (scanline_y < mid.y) {
		u1 = (mid.y - scanline_y) / (mid.y - min.y);
		x1_e = mid.x + u1 * (min.x - mid.x);
	} else {
		x1_e = mid.x;
	}
	
	u2 = (scanline_y - min.y) / (max.y - min.y);
	x2_e = min.x + u2 * (max.x - min.x);

	xmin = x1_e < x2_e ? x1_e : x2_e;
	xmax = x1_e < x2_e ? x2_e : x1_e;
}

void fillTriangle(Obj * o, void(*fill)(int,int)) 
{
	if (C.fillMode == fillMode::FLAT_TRIANGLE) {
		// Sort points by y coordinates.
		int p1 = o->pts[0].y > o->pts[1].y? 0 : 1,
			p2 = p1 == 1 ? 0 : 1; // p1 > p2
		Point min, max, mid;
		if (o->pts[2].y > o->pts[p1].y) {
			max = o->pts[2];
			mid = o->pts[p1];
			min = o->pts[p2];
		} else if (o->pts[2].y > o->pts[p2].y) {
			max = o->pts[p1];
			mid = o->pts[2];
			min = o->pts[p2];
		} else {
			max = o->pts[p1];
			mid = o->pts[p2];
			min = o->pts[2];
		}

		float xmin, xmax;

		// Let's start from middle point and scan the triangle.
		calcX(min, max, mid, mid.y, xmin, xmax);
		for(int xpos = int(xmin); xpos <= int(xmax); xpos++) {
			fill(xpos, mid.y);
		}
		
		if (mid.y > min.y) { // Do scan from ymid |->| ymin
			for(int ypos = int(mid.y); ypos >= int(min.y); ypos --) {
				calcX(min, max, mid, ypos, xmin, xmax);
				for(int xpos = int(xmin); xpos <= int(xmax); xpos ++) {
					fill(xpos, ypos);
				}
			}
		}
		
		if (mid.y < max.y) { // Do scan from ymid |->| ymax
			for(int ypos = int(mid.y); ypos <= int(max.y); ypos ++) {
				calcX(min, max, mid, ypos, xmin, xmax);
				for(int xpos = int(xmin); xpos <= int(xmax); xpos ++) {
					fill(xpos, ypos);
				}
			}
		}
	} else if (C.fillMode == fillMode::HALF_SPACE) {
		// Find out clockwise point sequence.
		Point p[3];
		for (int i = 0; i < 3; i ++) {
			if (o->pts[i].y < o->pts[(i+1)%3].y) {
				if (o->pts[i].x < o->pts[(i+2)%3].x) {
					p[0] = o->pts[i];
					p[1] = o->pts[(i+1)%3];
					p[2] = o->pts[(i+2)%3];
					break;
				}
			}
			if (o->pts[i].y < o->pts[(i+2)%3].y) {
				if (o->pts[i].x < o->pts[(i+1)%3].x) {
					p[0] = o->pts[i];
					p[1] = o->pts[(i+2)%3];
					p[2] = o->pts[(i+1)%3];
					break;
				}
			}
		}

		Point e[3] = { // Vectors for all edges
			p[1].x - p[0].x, p[1].y - p[0].y,
			p[2].x - p[1].x, p[2].y - p[1].y,
			p[0].x - p[2].x, p[0].y - p[2].y };
		Point N[3] = { // Normal vectors for all edges
			-e[0].y, e[0].x,
			-e[1].y, e[1].x,
			-e[2].y, e[2].x	};

		// Get the rect of the scan area
		int xmin = (p[0].x < p[1].x ? p[0].x : p[1].x) < p[2].x ?
			(p[0].x < p[1].x ? p[0].x : p[1].x) : p[2].x;
		int xmax = (p[0].x > p[1].x ? p[0].x : p[1].x) > p[2].x ?
			(p[0].x > p[1].x ? p[0].x : p[1].x) : p[2].x;
		int ymin = (p[0].y < p[1].y ? p[0].y : p[1].y) < p[2].y ?
			(p[0].y < p[1].y ? p[0].y : p[1].y) : p[2].y;
		int ymax = (p[0].y > p[1].y ? p[0].y : p[1].y) > p[2].y ?
			(p[0].y > p[1].y ? p[0].y : p[1].y) : p[2].y;
	
		for(int y = ymin; y <= ymax; y ++) {
			for(int x = xmin; x <= xmax; x++) {
				bool isFill = true;
				for(int i = 0; i < 3; i ++) {
					if (N[i].x * (x - p[i].x) + N[i].y * (y - p[i].y) > 0) {
						isFill = false;
						break;
					}
				}
				if (isFill) {
					fill(x, y);
				}
			}
		}
	}
}

void drawFilledTriangle(Obj *o)
{
	if (C.displayMode == displayMode::SCANLINE) { fillTriangle(o, stripeFill); }
	else { fillTriangle(o, solidFill); }
}

void drawTriangleImage(Obj *o)
{
	fillTriangle(o,imageFill); //image fill uses the image pixels
}

void changeDisplayMode() {
	C.displayMode = (C.displayMode + 1) % 3;
	printf("[INFO]Display Mode changed!\n");
}

void changeFillMode() {
	C.fillMode = (C.fillMode + 1) % NUM_OF_FILL_MODE;
	printf("[INFO]Fill Mode changed!\n");
}

void title() {
	  char str[200] = "", fmode[20], dmode[20];
	  switch (C.fillMode) {
	  case fillMode::FLAT_TRIANGLE:
		  sprintf(fmode, "%s", "Flat triangle");
		  break;
	  case fillMode::HALF_SPACE:
		  sprintf(fmode, "%s", "Half Space");
		  break;
	  default:;
	  }
	  switch (C.displayMode) {
	  case displayMode::TEXTURE:
		  sprintf(dmode, "%s", "Texture");
		  break;
	  case displayMode::SCANLINE:
		  sprintf(dmode, "%s", "Scanline");
		  break;
	  case displayMode::SOLID:
		  sprintf(dmode, "%s", "Solid");
		  break;
	  default:;
	  }
	  sprintf(str, "snake | Fill mode: %s | Display mode: %s | Snake pos:(%d,%d)", fmode, dmode, int(S.pts[0].x), int(S.pts[0].y));
	  glutSetWindowTitle(str);
}