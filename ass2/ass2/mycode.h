#pragma once


////////////////////////////////////////////////////////////////////////////////
// data structures
////////////////////////////////////////////////////////////////////////////////
typedef unsigned char Byte;

typedef struct
{
  Byte r,g,b;
} RGBPixel, **RGBImg;

typedef struct
{
  RGBPixel **data;
  int   rows,cols;
} Image;

typedef struct{
  float x,y;      // x and y coordinates of the point
} Point;

typedef enum {NOOBJ, RECTOBJ, CIRC, PLINE, PGON} objType;

typedef struct{
  int   type;     // object type
  float tx,ty;    // translation
  float rotAng;   // rotation
  float sx,sy;    // scale
  float r,g,b;    // color
  float fillFlag; // fill flag
  int   ptsNum;   // number of coordinates in the vertex array
  float param;    // shape parameter (e.g. radius of circle)
  Point pts[100]; // polygon vertices
} Obj;

typedef enum {LEFT, RIGHT, UP, DOWN, NIL} snakeDir;

typedef struct
{
  int len;              // snake length
  int width;            // snake width
  int speed;            // snake speed
  int speedIncr;        // speed increment
  int widthIncr;        // width increment
  RGBPixel color;       // snake color 
  Point pts[10000];     // snake points
} Snake;

typedef struct
{
  Image fbuff;                  // frame buffer (the game board)
  Image texture;                // image texture
  RGBPixel fillColor;           // fill color used by setPixel
  RGBPixel clearColor;          // clear color used by clearFbuff
  int fps;                      // frames per second
  unsigned long frameNum;       // frame number
  float event1rate;             // length increase rate in seconds
  float event2rate;             // speed increase rate in seconds
  float event3rate;             // width increase rate in seconds
  int displayMode;              // display mode (texture/scanline)
  int fillMode;					// fill mode (different algorithms)
  int gameOver;                 // game over flag 
  int **dirs;                   // record of direction changes
  int objNum;                   // the total number of polygon objects
  Obj objLst[100];              // object (polygon) list
} Context;


////////////////////////////////////////////////////////////////////
// Code implemented by me
////////////////////////////////////////////////////////////////////

#define NUM_OF_FILL_MODE 2
typedef enum {FLAT_TRIANGLE, HALF_SPACE} fillMode;
typedef enum {TEXTURE, SCANLINE, SOLID} displayMode;

void fillTriangle(Obj * o, void(*fill)(int, int));
void drawFilledTriangle(Obj *o);
void drawTriangleImage(Obj *o);
void changeDisplayMode();
void changeFillMode();
void title();